var express = require("express");
var router = express.Router();
var multer = require("multer");
const { uuid } = require('uuidv4');
// const upload = multer({
//   dest: "upload",
//   limits: {
//     fieldSize: 1000000,
//   },
//   fileFilter(req, file, cb) {
//     if (!file.originalname.endsWith(".pdf"))
//       return cb(new Error("File format is incorect"));
//     cb(undefined, true);
//   }
// });
var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "upload");
  },
  filename: function (req, file, callback) {
    callback(null, uuid()+".pdf");
  },
});

var upload = multer({
  limits: {
    fieldSize: 1000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.endsWith(".pdf"))
      return cb(new Error("File format is incorect"));
    cb(undefined, true);
  },
  storage: storage,
});
/* GET home page. */
router.post(
  "/upload",
  upload.single("upload"),
  async (req, res, next) => {
    res.status(200).json({ message: "success", response: "api/download/"+req.file.filename });
  },
  (err, req, res, next) => res.status(404).json({ error: err.message })
);

router.get(
  "/download/:filename",
  async (req, res, next) => {
    res.download("upload/"+req.params.filename)
  },
  (err, req, res, next) => res.status(404).json({ message: err.message })
);

module.exports = router;
